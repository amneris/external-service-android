# Change Log

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## Unreleased
### Add
- Wrapper "get subscriptions" call for android sdk 
- Implement dummy responses for "get subscriptions" call, avoiding to call real endpoint
- Wrapper "get voided purchases" call for android sdk

