package com.abaenglish.external.android.objectmother;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.testing.auth.oauth2.MockGoogleCredential;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.androidpublisher.AndroidPublisher;
import com.google.api.services.androidpublisher.model.SubscriptionPurchase;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class GoogleObjectMother {

    public static SubscriptionPurchase subscriptionPurchase() {

        return new SubscriptionPurchase();
    }

    public static GoogleCredential googleCredential() {

        return new MockGoogleCredential.Builder().build();
    }

    public static AndroidPublisher androidPublisher() throws GeneralSecurityException, IOException {

        return new AndroidPublisher
                .Builder(
                GoogleNetHttpTransport.newTrustedTransport(),
                JacksonFactory.getDefaultInstance(),
                GoogleObjectMother.googleCredential()
        ).build();
    }
}
