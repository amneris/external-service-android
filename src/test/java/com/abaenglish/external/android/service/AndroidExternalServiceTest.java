package com.abaenglish.external.android.service;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.external.android.properties.AndroidProperties;
import com.abaenglish.external.android.service.component.AndroidSubscriptionPurchaseTest;
import com.abaenglish.external.android.service.dto.SubscriptionPurchaseResponse;
import com.abaenglish.external.android.service.exception.BadRequestAndroidServiceException;
import com.abaenglish.external.android.service.impl.AndroidExternalService;
import com.google.api.services.androidpublisher.model.SubscriptionPurchase;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

import static com.abaenglish.external.android.service.component.AndroidSubscriptionPurchaseTest.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;

public class AndroidExternalServiceTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private AndroidProperties androidProperties;

    private AndroidExternalService androidExternalService;
    @Autowired
    private IAndroidSubscriptionPurchase androidSubscriptionPurchase;

    @Before
    public void initMocks() {
        androidProperties = new AndroidProperties();
        // set to start application, really does not connect
        androidProperties.setProfile("test");
        androidProperties.setPackageName("package.test");
        androidProperties.setCredentialsJSON("123456789JSON");
    }

    @Test
    public void getSubscriptionPurchase() throws Exception {

        androidSubscriptionPurchase = new AndroidSubscriptionPurchaseTest();
        androidExternalService = new AndroidExternalService(androidSubscriptionPurchase);

        final SubscriptionPurchase expectedSubscriptionPurchase = testPurchase();
        final SubscriptionPurchaseResponse response = androidExternalService.getSubscriptionPurchase("good", "token");

        checkResponse(response, expectedSubscriptionPurchase, true);
    }

    @Test
    public void getSubscriptionPurchaseCancelled() throws Exception {

        androidSubscriptionPurchase = new AndroidSubscriptionPurchaseTest();
        androidExternalService = new AndroidExternalService(androidSubscriptionPurchase);

        final SubscriptionPurchase expectedSubscriptionPurchase = testPurchaseCancelled();
        final SubscriptionPurchaseResponse response = androidExternalService.getSubscriptionPurchase("cancelled_past", "token");

        checkResponse(response, expectedSubscriptionPurchase, false);
    }

    @Test
    public void getSubscriptionPurchaseCancelRecurring() throws Exception {

        androidSubscriptionPurchase = new AndroidSubscriptionPurchaseTest();
        androidExternalService = new AndroidExternalService(androidSubscriptionPurchase);

        final SubscriptionPurchase expectedSubscriptionPurchase = testPurchaseCancelRecurring();
        final SubscriptionPurchaseResponse response = androidExternalService.getSubscriptionPurchase("cancelled_recurring", "token");

        checkResponse(response, expectedSubscriptionPurchase, false);
    }

    private void checkResponse(final SubscriptionPurchaseResponse response, final SubscriptionPurchase expectedSubscriptionPurchase, boolean isCurrentDate) {
        assertNotNull(response);
        Assertions.assertThat(response.getAutoRenewing()).isEqualTo(expectedSubscriptionPurchase.getAutoRenewing());
        Assertions.assertThat(response.getCountryCode()).isEqualTo(expectedSubscriptionPurchase.getCountryCode());
        Assertions.assertThat(response.getDeveloperPayload()).isEqualTo(expectedSubscriptionPurchase.getDeveloperPayload());
        Assertions.assertThat(response.getKind()).isEqualTo(expectedSubscriptionPurchase.getKind());
        Assertions.assertThat(response.getPaymentState()).isEqualTo(expectedSubscriptionPurchase.getPaymentState());
        Assertions.assertThat(response.getPriceAmountMicros()).isEqualTo(expectedSubscriptionPurchase.getPriceAmountMicros());
        Assertions.assertThat(response.getPriceCurrencyCode()).isEqualTo(expectedSubscriptionPurchase.getPriceCurrencyCode());

        if(isCurrentDate) {
            // accurate datetime to date
            Assertions.assertThat(longToLocalDate(response.getStartTimeMillis())).isEqualTo(longToLocalDate(expectedSubscriptionPurchase.getStartTimeMillis()));
            Assertions.assertThat(longToLocalDate(response.getExpiryTimeMillis())).isEqualTo(longToLocalDate(expectedSubscriptionPurchase.getExpiryTimeMillis()));

        } else {
            Assertions.assertThat(response.getStartTimeMillis()).isEqualTo(expectedSubscriptionPurchase.getStartTimeMillis());
            Assertions.assertThat(response.getExpiryTimeMillis()).isEqualTo(expectedSubscriptionPurchase.getExpiryTimeMillis());
        }

        if(expectedSubscriptionPurchase.getCancelReason() == null) {
            Assertions.assertThat(response.getCancelReason()).isNull();
        } else {
            Assertions.assertThat(response.getCancelReason()).isEqualTo(expectedSubscriptionPurchase.getCancelReason());
        }
        if(response.getUserCancellationTimeMillis() == null) {
            Assertions.assertThat(response.getUserCancellationTimeMillis()).isNull();
        } else {
            Assertions.assertThat(response.getUserCancellationTimeMillis()).isEqualTo(expectedSubscriptionPurchase.getUserCancellationTimeMillis());
        }
    }

    private LocalDate longToLocalDate(final long epoch) {
        return Instant.ofEpochMilli(epoch).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    @Test(expected = ServiceException.class)
    public void getSubscriptionPurchaseError() throws Exception {
        androidSubscriptionPurchase = new AndroidSubscriptionPurchaseTest();

        androidExternalService = new AndroidExternalService(androidSubscriptionPurchase);

        SubscriptionPurchaseResponse response = androidExternalService.getSubscriptionPurchase("bad", "token");
        assertNull(response);

    }

    @Test(expected = BadRequestAndroidServiceException.class)
    public void getSubscriptionPurchaseErrorRejected() throws Exception {
        androidSubscriptionPurchase = new AndroidSubscriptionPurchaseTest();

        androidExternalService = new AndroidExternalService(androidSubscriptionPurchase);

        SubscriptionPurchaseResponse response = androidExternalService.getSubscriptionPurchase("rejected", "token");
        assertNull(response);

    }

    @Test(expected = ServiceException.class)
    public void getSubscriptionPurchaseErrorSubscriptionId() throws Exception {
        androidSubscriptionPurchase = new AndroidSubscriptionPurchaseTest();

        androidExternalService = new AndroidExternalService(androidSubscriptionPurchase);

        SubscriptionPurchaseResponse response = androidExternalService.getSubscriptionPurchase(null, "token");
        assertNull(response);

    }

    @Test(expected = ServiceException.class)
    public void getSubscriptionPurchaseErrorToken() throws Exception {
        androidSubscriptionPurchase = Mockito.mock(AndroidSubscriptionPurchaseTest.class);

        androidExternalService = new AndroidExternalService(androidSubscriptionPurchase);

        doThrow(ServiceException.class).when(androidSubscriptionPurchase).getSubscriptionPurchase("bad", null);

        SubscriptionPurchaseResponse response = androidExternalService.getSubscriptionPurchase("bad", null);
        assertNull(response);

    }

}
