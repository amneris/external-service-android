package com.abaenglish.external.android.autoconfigure;

import com.abaenglish.external.android.properties.AndroidProperties;
import com.abaenglish.external.android.service.IAndroidSubscriptionPurchase;
import com.abaenglish.external.android.service.component.AndroidSubscriptionPurchase;
import com.abaenglish.external.android.service.component.AndroidSubscriptionPurchaseTest;
import com.abaenglish.external.android.service.impl.AndroidAuthenticationService;
import com.abaenglish.external.android.service.impl.AndroidExternalService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

@Import({AndroidProperties.class, AndroidExternalService.class, AndroidAuthenticationService.class})
public class ExternalServiceAndroidAutoConfiguration {

    @Bean(name = "androidSubscriptionPurchase")
    @ConditionalOnProperty(prefix = "external.android", name = "profile", havingValue = "pro")
    public IAndroidSubscriptionPurchase pro(AndroidProperties androidProperties, AndroidAuthenticationService authenticationService) {
        return new AndroidSubscriptionPurchase(androidProperties, authenticationService);
    }

    @Bean(name = "androidSubscriptionPurchase")
    @ConditionalOnProperty(prefix = "external.android", name = "profile", havingValue = "test")
    public IAndroidSubscriptionPurchase test() {
        return new AndroidSubscriptionPurchaseTest();
    }

}
