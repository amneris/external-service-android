package com.abaenglish.external.android.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "external.android")
public class AndroidProperties {

    private String profile;
    private String packageName;
    private String credentialsJSON;

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getCredentialsJSON() {
        return credentialsJSON;
    }

    public void setCredentialsJSON(String credentialsJSON) {
        this.credentialsJSON = credentialsJSON;
    }
}
