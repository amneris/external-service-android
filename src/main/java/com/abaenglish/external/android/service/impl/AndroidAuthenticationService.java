package com.abaenglish.external.android.service.impl;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.external.android.properties.AndroidProperties;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.androidpublisher.AndroidPublisher;
import com.google.api.services.androidpublisher.AndroidPublisherScopes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.Collections;

import static com.abaenglish.external.android.service.exception.ErrorMessages.ERROR_AUTHENTICATION;
import static com.abaenglish.external.android.service.exception.ErrorMessages.ERROR_CREDENTIALS;

public class AndroidAuthenticationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AndroidAuthenticationService.class);
    /**
     * Global instance of the JSON factory.
     */
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    /**
     * Global instance of the HTTP transport.
     */
    private HttpTransport HTTP_TRANSPORT;
    @Autowired
    private AndroidProperties androidProperties;

    /**
     * Performs all necessary setup steps for running requests against the API.
     *
     * @return the {@Link AndroidPublisher} service
     * @throws GeneralSecurityException
     * @throws IOException
     */
    public AndroidPublisher init() throws ServiceException {

        try {

            if (HTTP_TRANSPORT == null) {
                HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            }

            // Authorization.
            Credential credential = authorizeWithServiceAccount();

            // Set up and return API client.
            return new AndroidPublisher.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(
                    androidProperties.getPackageName()).build();
        } catch (GeneralSecurityException e) {
            LOGGER.error("Can't access for security in Google Credentials: " + e.getMessage(), e);
            throw new ServiceException(ERROR_CREDENTIALS.getError(), e);
        } catch (IOException e) {
            LOGGER.error("Something wrong when try create Google credentials or AndroidPublisher: " + e.getMessage(), e);
            throw new ServiceException(ERROR_AUTHENTICATION.getError(), e);
        }
    }

    public Credential authorizeWithServiceAccount() throws ServiceException, IOException {

        InputStream stream = new ByteArrayInputStream(androidProperties.getCredentialsJSON().getBytes(StandardCharsets.UTF_8));

        return GoogleCredential.fromStream(stream).createScoped(Collections.singleton(AndroidPublisherScopes.ANDROIDPUBLISHER));
    }
}
