package com.abaenglish.external.android.service.dto;

public class SubscriptionPurchaseResponse {

    private Boolean autoRenewing;
    private String countryCode;
    private String developerPayload;
    private Long expiryTimeMillis;
    private String kind;
    private Integer paymentState;
    private Long priceAmountMicros;
    private String priceCurrencyCode;
    private Long startTimeMillis;
    private Integer cancelReason;
    private Long userCancellationTimeMillis;

    public Boolean getAutoRenewing() {
        return autoRenewing;
    }

    public void setAutoRenewing(Boolean autoRenewing) {
        this.autoRenewing = autoRenewing;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getDeveloperPayload() {
        return developerPayload;
    }

    public void setDeveloperPayload(String developerPayload) {
        this.developerPayload = developerPayload;
    }

    public Long getExpiryTimeMillis() {
        return expiryTimeMillis;
    }

    public void setExpiryTimeMillis(Long expiryTimeMillis) {
        this.expiryTimeMillis = expiryTimeMillis;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public Integer getPaymentState() {
        return paymentState;
    }

    public void setPaymentState(Integer paymentState) {
        this.paymentState = paymentState;
    }

    public Long getPriceAmountMicros() {
        return priceAmountMicros;
    }

    public void setPriceAmountMicros(Long priceAmountMicros) {
        this.priceAmountMicros = priceAmountMicros;
    }

    public String getPriceCurrencyCode() {
        return priceCurrencyCode;
    }

    public void setPriceCurrencyCode(String priceCurrencyCode) {
        this.priceCurrencyCode = priceCurrencyCode;
    }

    public Long getStartTimeMillis() {
        return startTimeMillis;
    }

    public void setStartTimeMillis(Long startTimeMillis) {
        this.startTimeMillis = startTimeMillis;
    }

    public Integer getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(Integer cancelReason) {
        this.cancelReason = cancelReason;
    }

    public Long getUserCancellationTimeMillis() {
        return userCancellationTimeMillis;
    }

    public void setUserCancellationTimeMillis(Long userCancellationTimeMillis) {
        this.userCancellationTimeMillis = userCancellationTimeMillis;
    }

    public static final class Builder {
        public Boolean autoRenewing;
        public String countryCode;
        public String developerPayload;
        public Long expiryTimeMillis;
        public String kind;
        public Integer paymentState;
        public Long priceAmountMicros;
        public String priceCurrencyCode;
        public Long startTimeMillis;
        public Integer cancelReason;
        private Long userCancellationTimeMillis;

        private Builder() {
        }

        public static Builder aSubscriptionPurchaseResponse() {
            return new Builder();
        }

        public Builder autoRenewing(Boolean autoRenewing) {
            this.autoRenewing = autoRenewing;
            return this;
        }

        public Builder countryCode(String countryCode) {
            this.countryCode = countryCode;
            return this;
        }

        public Builder developerPayload(String developerPayload) {
            this.developerPayload = developerPayload;
            return this;
        }

        public Builder expiryTimeMillis(Long expiryTimeMillis) {
            this.expiryTimeMillis = expiryTimeMillis;
            return this;
        }
        public Builder userCancellationTimeMillis(Long userCancellationTimeMillis) {
            this.userCancellationTimeMillis = userCancellationTimeMillis;
            return this;
        }

        public Builder kind(String kind) {
            this.kind = kind;
            return this;
        }

        public Builder paymentState(Integer paymentState) {
            this.paymentState = paymentState;
            return this;
        }

        public Builder priceAmountMicros(Long priceAmountMicros) {
            this.priceAmountMicros = priceAmountMicros;
            return this;
        }

        public Builder priceCurrencyCode(String priceCurrencyCode) {
            this.priceCurrencyCode = priceCurrencyCode;
            return this;
        }

        public Builder startTimeMillis(Long startTimeMillis) {
            this.startTimeMillis = startTimeMillis;
            return this;
        }

        public Builder cancelReason(Integer cancelReason) {
            this.cancelReason = cancelReason;
            return this;
        }

        public SubscriptionPurchaseResponse build() {
            SubscriptionPurchaseResponse subscriptionPurchaseResponse = new SubscriptionPurchaseResponse();
            subscriptionPurchaseResponse.setAutoRenewing(autoRenewing);
            subscriptionPurchaseResponse.setCountryCode(countryCode);
            subscriptionPurchaseResponse.setDeveloperPayload(developerPayload);
            subscriptionPurchaseResponse.setExpiryTimeMillis(expiryTimeMillis);
            subscriptionPurchaseResponse.setKind(kind);
            subscriptionPurchaseResponse.setPaymentState(paymentState);
            subscriptionPurchaseResponse.setPriceAmountMicros(priceAmountMicros);
            subscriptionPurchaseResponse.setPriceCurrencyCode(priceCurrencyCode);
            subscriptionPurchaseResponse.setStartTimeMillis(startTimeMillis);
            subscriptionPurchaseResponse.setCancelReason(cancelReason);
            subscriptionPurchaseResponse.setUserCancellationTimeMillis(userCancellationTimeMillis);
            return subscriptionPurchaseResponse;
        }
    }
}
