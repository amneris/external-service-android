package com.abaenglish.external.android.service.impl;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.external.android.service.IAndroidExternalService;
import com.abaenglish.external.android.service.IAndroidSubscriptionPurchase;
import com.abaenglish.external.android.service.dto.SubscriptionPurchaseResponse;
import com.google.api.services.androidpublisher.model.SubscriptionPurchase;
import com.google.api.services.androidpublisher.model.VoidedPurchase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;

import static com.abaenglish.external.android.service.exception.ErrorMessages.ILLEGAL_PARAMS_ANDROID;

@Service
public class AndroidExternalService implements IAndroidExternalService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AndroidExternalService.class);

    private final IAndroidSubscriptionPurchase androidSubscriptionPurchase;

    @Autowired
    public AndroidExternalService(IAndroidSubscriptionPurchase androidSubscriptionPurchase) {
        this.androidSubscriptionPurchase = androidSubscriptionPurchase;
    }

    @Override
    public SubscriptionPurchaseResponse getSubscriptionPurchase(String subscriptionId, String token) throws ServiceException {
        try {
            Assert.notNull(subscriptionId, "subscriptionId identifier is mandatory");
            Assert.notNull(token, "token is mandatory");
        } catch (IllegalArgumentException e) {
            LOGGER.warn("Params aren't correct for obtain purchase '{}'", e.getMessage());
            throw new ServiceException(ILLEGAL_PARAMS_ANDROID.getError(), e);
        }

        SubscriptionPurchase response = androidSubscriptionPurchase.getSubscriptionPurchase(subscriptionId, token);
        LOGGER.debug("Android SubscriptionPurchase found: {}", response.toString());
        return SubscriptionPurchaseResponse.Builder.aSubscriptionPurchaseResponse()
                .autoRenewing(response.getAutoRenewing())
                .cancelReason(response.getCancelReason())
                .countryCode(response.getCountryCode())
                .developerPayload(response.getDeveloperPayload())
                .expiryTimeMillis(response.getExpiryTimeMillis())
                .kind(response.getKind())
                .paymentState(response.getPaymentState())
                .priceAmountMicros(response.getPriceAmountMicros())
                .priceCurrencyCode(response.getPriceCurrencyCode())
                .startTimeMillis(response.getStartTimeMillis())
                .userCancellationTimeMillis(response.getUserCancellationTimeMillis())
                .build();
    }

    @Override
    public List<VoidedPurchase> getVoidedPurchases(Optional<Long> startTime, Optional<Long> endTime) throws ServiceException {
        return androidSubscriptionPurchase.getVoidedpurchases(startTime, endTime);
    }
}
