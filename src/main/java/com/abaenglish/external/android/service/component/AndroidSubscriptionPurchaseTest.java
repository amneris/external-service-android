package com.abaenglish.external.android.service.component;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.external.android.service.IAndroidSubscriptionPurchase;
import com.abaenglish.external.android.service.exception.BadRequestAndroidServiceException;
import com.google.api.services.androidpublisher.model.SubscriptionPurchase;
import com.google.api.services.androidpublisher.model.VoidedPurchase;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

public class AndroidSubscriptionPurchaseTest implements IAndroidSubscriptionPurchase {
    /***
     *
     * Depende del tipo de subscriptionId que nos manden devolveremos los tipos de errores que tenemos.
     *
     * @param subscriptionId
     * @param token
     * @return
     * @throws ServiceException
     */
    @Override
    public SubscriptionPurchase getSubscriptionPurchase(String subscriptionId, String token) throws ServiceException {
        SubscriptionPurchase subscriptionPurchase;
        Exception e = new Exception("Error Test");

        switch (subscriptionId) {
            case "bad":
                throw new ServiceException("There was an error obtaining Android Purchase.", e);
            case "rejected":
                throw new BadRequestAndroidServiceException(e.getMessage(),e);
            case "cancelled_past":
                subscriptionPurchase = testPurchaseCancelled();
                break;
            case "cancelled_recurring":
                subscriptionPurchase = testPurchaseCancelRecurring();
                break;
            case "good":
            default:
                subscriptionPurchase = testPurchase();
                break;
        }

        return subscriptionPurchase;
    }

    @Override
    public List<VoidedPurchase> getVoidedpurchases(Optional<Long> startTime, Optional<Long> endTime) throws ServiceException {
        return null;
    }

    private static Long getCurrentTime(final Optional<Long> months) {
        if(months.isPresent()) {
            return LocalDateTime.now().plusMonths(months.get()).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        } else {
            return LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        }
    }

    public static SubscriptionPurchase testPurchase() {
        SubscriptionPurchase purchase = new SubscriptionPurchase();
        purchase.setPriceAmountMicros(44990000L);
        purchase.setAutoRenewing(true);
        purchase.setCountryCode("FR");
        purchase.setDeveloperPayload("");
        purchase.setExpiryTimeMillis(getCurrentTime(Optional.of(1L)));
        purchase.setKind("androidpublisher#subscriptionPurchase");
        purchase.setPaymentState(1);
        purchase.setPriceCurrencyCode("EUR");
        purchase.setStartTimeMillis(getCurrentTime(Optional.empty()));
        return purchase;
    }

    public static SubscriptionPurchase testPurchaseCancelled() {
        SubscriptionPurchase purchase = new SubscriptionPurchase();
        purchase.setPriceAmountMicros(64990000L);
        purchase.setAutoRenewing(false);
        purchase.setCountryCode("ES");
        purchase.setDeveloperPayload("");
        purchase.setExpiryTimeMillis(1491558738510L); //    4/7/17 9:52 AM
        purchase.setKind("androidpublisher#subscriptionPurchase");
        purchase.setPriceCurrencyCode("EUR");
        purchase.setStartTimeMillis(1490873261257L); //     3/30/17 11:27 AM
        purchase.setCancelReason(1);
        return purchase;
    }

    public static SubscriptionPurchase testPurchaseCancelRecurring() {
        SubscriptionPurchase purchase = new SubscriptionPurchase();
        purchase.setPriceAmountMicros(192380000L);
        purchase.setAutoRenewing(false);
        purchase.setCountryCode("PL");
        purchase.setDeveloperPayload("");
        purchase.setExpiryTimeMillis(1493506737814L);//     4/29/17 10:58 PM
        purchase.setKind("androidpublisher#subscriptionPurchase");
        purchase.setPaymentState(1);
        purchase.setPriceCurrencyCode("PLN");
        purchase.setStartTimeMillis(1461970782314L);//      4/29/16 10:59 PM
        purchase.setCancelReason(0);
        purchase.setUserCancellationTimeMillis(1478702009559L); //    11/9/16 2:33 PM
        return purchase;
    }

}
