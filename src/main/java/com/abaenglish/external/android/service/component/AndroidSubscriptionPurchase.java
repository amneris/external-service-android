package com.abaenglish.external.android.service.component;

import com.abaenglish.boot.exception.CodeMessage;
import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.external.android.properties.AndroidProperties;
import com.abaenglish.external.android.service.IAndroidSubscriptionPurchase;
import com.abaenglish.external.android.service.exception.BadRequestAndroidServiceException;
import com.abaenglish.external.android.service.impl.AndroidAuthenticationService;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.services.androidpublisher.AndroidPublisher;
import com.google.api.services.androidpublisher.model.SubscriptionPurchase;
import com.google.api.services.androidpublisher.model.VoidedPurchase;
import com.google.api.services.androidpublisher.model.VoidedPurchasesListResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;

import static com.abaenglish.external.android.service.exception.ErrorMessages.ERROR_PURCHASE_ANDROID;

public class AndroidSubscriptionPurchase implements IAndroidSubscriptionPurchase {

    private static final Logger LOGGER = LoggerFactory.getLogger(AndroidSubscriptionPurchase.class);
    private final AndroidProperties androidProperties;
    private final AndroidAuthenticationService authenticationService;

    public AndroidSubscriptionPurchase(AndroidProperties androidProperties, AndroidAuthenticationService authenticationService) {
        this.androidProperties = androidProperties;
        this.authenticationService = authenticationService;
    }

    @Override
    public SubscriptionPurchase getSubscriptionPurchase(String subscriptionId, String token) throws ServiceException {

        AndroidPublisher publisher = authenticationService.init();

        AndroidPublisher.Purchases.Subscriptions subscriptions = publisher.purchases().subscriptions();

        try {
            AndroidPublisher.Purchases.Subscriptions.Get subscription = subscriptions.get(androidProperties.getPackageName(),
                    subscriptionId, token);
            return subscription.execute();
        }
        catch (GoogleJsonResponseException e) {
            LOGGER.error(e.getMessage(), e);
            if(e.getStatusCode() == 400){
                throw new BadRequestAndroidServiceException(new CodeMessage("ANDROID400", e.getMessage()), e);
            } else {
                throw new ServiceException(e.getMessage(), e);
            }
        }
        catch (Exception e) {
            if (e instanceof ServiceException) {
                throw (ServiceException) e;
            } else {
                LOGGER.error("There was an error obtaining Android Purchase : " + e.getMessage(), e);
                throw new ServiceException(ERROR_PURCHASE_ANDROID.getError(), e);
            }
        }

    }

    @Override
    public List<VoidedPurchase> getVoidedpurchases(Optional<Long> startTime, Optional<Long> endTime) throws ServiceException {
        AndroidPublisher publisher = authenticationService.init()  ;

        AndroidPublisher.Purchases.Voidedpurchases publisherVoidedpurchases = publisher.purchases().voidedpurchases();

        try {
            AndroidPublisher.Purchases.Voidedpurchases.List list = publisherVoidedpurchases.list(androidProperties.getPackageName());
            startTime.ifPresent( t -> list.setStartTime(t));
            endTime.ifPresent( t -> list.setEndTime(t));
            VoidedPurchasesListResponse execute = list.execute();
            List<VoidedPurchase> voidedPurchases = execute.getVoidedPurchases();
            return voidedPurchases;
        }
        catch (GoogleJsonResponseException e) {
            LOGGER.error(e.getMessage(), e);
            if(e.getStatusCode() == 400){
                throw new BadRequestAndroidServiceException(new CodeMessage("ANDROID400", e.getMessage()), e);
            } else {
                throw new ServiceException(e.getMessage(), e);
            }
        }
        catch (Exception e) {
            if (e instanceof ServiceException) {
                throw (ServiceException) e;
            } else {
                LOGGER.error("There was an error obtaining Android Voidedpurchases : " + e.getMessage(), e);
                throw new ServiceException(ERROR_PURCHASE_ANDROID.getError(), e);
            }
        }

    }
}
