package com.abaenglish.external.android.service.exception;

import com.abaenglish.boot.exception.CodeMessage;



public enum ErrorMessages {

    ILLEGAL_PARAMS_ANDROID(new CodeMessage("ANDROID001", "Illegal params aren't correct for obtain purchase")),
    ERROR_PURCHASE_ANDROID(new CodeMessage("ANDROID002", "There was an error obtaining Android Purchase.")),
    ERROR_CREDENTIALS(new CodeMessage("ANDROID003", "Can't access for security in Google Credentials.")),
    ERROR_AUTHENTICATION(new CodeMessage("ANDROID004", "Something wrong when try create Google credentials or AndroidPublisher."));

    private final CodeMessage error;

    ErrorMessages(CodeMessage errorMessage) {
        error = new CodeMessage(errorMessage.getCode(), errorMessage.getMessage());
    }

    public CodeMessage getError() {
        return error;
    }

}
