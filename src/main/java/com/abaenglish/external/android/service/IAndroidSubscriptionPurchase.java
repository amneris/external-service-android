package com.abaenglish.external.android.service;

import com.abaenglish.boot.exception.service.ServiceException;
import com.google.api.services.androidpublisher.model.SubscriptionPurchase;
import com.google.api.services.androidpublisher.model.VoidedPurchase;

import java.util.List;
import java.util.Optional;

public interface IAndroidSubscriptionPurchase {

    SubscriptionPurchase getSubscriptionPurchase(String productId, String token) throws ServiceException;

    List<VoidedPurchase> getVoidedpurchases(Optional<Long> startTime, Optional<Long> endTime) throws ServiceException;
}
