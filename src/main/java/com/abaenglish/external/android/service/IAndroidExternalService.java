package com.abaenglish.external.android.service;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.external.android.service.dto.SubscriptionPurchaseResponse;
import com.google.api.services.androidpublisher.model.VoidedPurchase;

import java.util.List;
import java.util.Optional;

public interface IAndroidExternalService {

    SubscriptionPurchaseResponse getSubscriptionPurchase(String productId, String token) throws ServiceException;

    List<VoidedPurchase> getVoidedPurchases(Optional<Long> startTime, Optional<Long> endTime) throws ServiceException;
}
