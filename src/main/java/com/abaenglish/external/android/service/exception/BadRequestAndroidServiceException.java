package com.abaenglish.external.android.service.exception;

import com.abaenglish.boot.exception.CodeMessage;
import com.abaenglish.boot.exception.service.ServiceException;

public class BadRequestAndroidServiceException extends ServiceException {

    public BadRequestAndroidServiceException(String message, Throwable t) {
        super(message, t);
    }

    public BadRequestAndroidServiceException(CodeMessage message, Throwable t) {
        super(message, t);
    }
}
